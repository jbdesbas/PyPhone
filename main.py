from machine import Pin, PWM
import time

beeper = PWM(Pin(14, Pin.OUT), freq=0, duty=512)
decrochePin=Pin(2, Pin.IN)

class Melody:
    def __init__ (self,notes,rhythm):
        self.notes=notes
        self.rhythm=rhythm
    

class Phone:
    def __init__(self,beeperPin,decrochePin):
        self.beeperPin=beeperPin
        self.beeper=PWM(Pin(14, Pin.OUT), freq=0, duty=512)
        self.decrochePin=decrochePin
        
    def isDecroche():
        if self.decrochePin.value()==1:
            return True
        else:
            return False
    
    def playMelody(melody):
        if !isinstance(melody,Melody):
            raise TypeError
        for tone, length in zip(melody.notes, melody.rhythm):
            beeper.freq(tones[tone])
            time.sleep(tempo/length)
        beeper.freq(0)
        beeper.deinit()
        


